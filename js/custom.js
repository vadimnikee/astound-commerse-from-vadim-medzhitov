$(document).ready(function(){
    function popup(){

            $(".show-more").click(function(){
                $('.overlay').removeClass('active');
                $(this).parent().parent().find('.overlay').addClass('active');
                $("body").addClass("show-overlay");
            });

            $(".to-close").click(function(){
                $("body").removeClass("show-overlay");
            });

    }
    popup();

    function shareSocial(){
        $("#share, #share2, #share3, #share4, #share5, #share6").jsSocials({
            url: "http://www.google.com",
            text: "Google Search Page",
            showLabel: false,
            showCount: "inside",
            shares: ["facebook", "twitter", "pinterest"]
        });
    }
    shareSocial();
    var isClosed=true;
    function openShare(){
        $('.product-view .share-container').css('display', 'none');
        var closed = $('.product-view .share-container').css('display', 'none');
        $(".product-view .share").each(function(){
            $(this).click(function(){
                if(isClosed){
                    $(this).addClass('close').attr('title', 'Close shares');
                    $(this).find('.share-container').fadeIn();
                    isClosed=false;
                    return true;
                } else{
                    $(this).removeClass('close').attr('title', 'Open shares');
                    $(this).find('.share-container').fadeOut();
                    isClosed=true;
                    return false;
                }
            })
        })

    }
    openShare();

    if($(window).width() < 1024){
        $(".pop-up form .input-container > label").text("Select a size:");
        function centerGrid(){
            var columnWidth = 305;
            var parentWidth = $('#masonry-container').parent().width();
            var rest = parentWidth%columnWidth;
            $('#masonry-container').css('padding-left', rest/2);
        }
        centerGrid();
        $(window).resize(centerGrid);
    }
});
